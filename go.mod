module heroes-service

go 1.15

require github.com/astaxie/beego v1.12.1

require (
	github.com/hyperledger/fabric-protos-go v0.0.0-20200819205323-f34c922b9e79 // indirect
	github.com/hyperledger/fabric-sdk-go v1.0.0-beta1
	github.com/pkg/errors v0.8.1
	github.com/shiena/ansicolor v0.0.0-20200830101100-9405ca8e49f3 // indirect
	github.com/smartystreets/goconvey v1.6.4
)

replace github.com/hyperledger/fabric-sdk-go => ../../hyperledger/fabric-sdk-go
