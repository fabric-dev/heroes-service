package dataStruct

type DataDigest struct {
	KeyId              string   `json:"keyId"`
	Vserion            string   `json:"version"`
	UserName           string   `json:"username"`
	OperationType      string   `json:"operationType"`
	DataType           string   `json:"dataType"`
	ServiceType        string   `json:"serviceType"`
	FileName           string   `json:"fileName"`
	FileSize           string   `json:"fileSize"`
	FileHash           string   `json:"fileHash"`
	URI                string   `json:"uri"`
	ParentKeyId        string   `json:"parentKeyId"`
	AttachmentFileUris []string `json:"attachmentFileUris"`
}
type VerifyRequest struct {
	KeyId    string `json:"keyId"`
	FileHash string `json:"fileHash"`
}

type Response struct {
	Success bool `json:"success"`
	Err    string `json:"err"`
}
type MismatchedSet struct {
	KeyId string `json:"keyId"`
	Msg   string `json:"msg"`
}
type ResponseVerify struct {
	Success bool            `json:"success"`
	Err     string          `json:"err"`
	Set     []MismatchedSet `json:"mismatchedSet"`
}
type BlockInfoResp struct {
	Success     bool `json:"success"`
	Err         string `json:"err"`
	TxId        string `json:"txId"`
	BlockHeight string `json:"blockHeight"`
	BlockHash   string `json:"blockHash"`
}
type Request struct {
	KeyId []string `json:"keyId"`
}
type ChainSet struct {
	KeySet [][]string `json:"chainSet"`
}
type TracedDataDigest struct {
	TracedList []DataDigest
}
