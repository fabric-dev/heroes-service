package controllers

import (
	"encoding/json"
	"fmt"
	"heroes-service/blockchain"
	"heroes-service/dataStruct"

	"github.com/astaxie/beego"
)

const badrequest int = 400
const success int = 200

type Application struct {
	beego.Controller
}
type FabricClient struct {
	Fabric *blockchain.FabricSetup
}

var client *FabricClient = &FabricClient{}

func Init(fabric *blockchain.FabricSetup) {
	client.Fabric = fabric
}
func (app *Application) ResponseJson(code int, data interface{}) {
	app.Ctx.Output.Status = code
	app.Data["json"] = data
	app.ServeJSON()
}
func (app *Application) StoreReceipt() {
	//check data is valid
	//unmarshal data
	fmt.Printf("receive store.......\n")
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var a dataStruct.DataDigest
	resp := &dataStruct.BlockInfoResp{
		Success:false,
	}
	if err := json.Unmarshal(body, &a); err != nil {
		resp.Err = fmt.Sprintf("数据格式错误")
		app.ResponseJson(badrequest, resp)
		return
	}
	fmt.Println("key:", a.KeyId)
	fmt.Println("uri:", a.URI)
	fmt.Println("filehash:", a.FileHash)
	if a.KeyId == "" || a.URI == "" || a.FileHash == "" {
		resp.Err = "keyId,fileHash,URI关键字段不能为空"
		app.ResponseJson(badrequest, resp)
		return
	}
	result, err := client.Fabric.Query("query", []byte(a.KeyId))
	if err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
	        return
	 }
	if len(result) != 0 || result != nil {
		resp.Err="数据已经上链，无需重复上链"
		app.ResponseJson(badrequest, resp)
		return
	}
	if a.ParentKeyId != "" {
		result, err := client.Fabric.QueryKey(a.ParentKeyId)
		if err != nil {
			resp.Err = "内部错误"
			fmt.Println("querykey 内部错误")
			app.ResponseJson(badrequest, resp)
			return
		}
		if len(result) == 0 || result == nil {
			resp.Err = "parentId不存在"
			app.ResponseJson(badrequest, resp)
			return
		}
	}
	txid, err := client.Fabric.InvokeData(body)
	if err != nil {
		resp.Err = fmt.Sprintf("store err:, %v\n", err)
		app.ResponseJson(badrequest, resp)
		return
	}
	resp.Success =true
	resp.TxId = string(txid)
	err = client.Fabric.GetBlockHeaderInfo(resp, txid)
	if err != nil {
		resp.Success = false
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	app.ResponseJson(success, resp)
}

func (app *Application) LoadReceipt() {
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var req dataStruct.VerifyRequest
	resp := dataStruct.Response{}
	resp.Success=false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = fmt.Sprintf("数据格式错误")
		app.ResponseJson(badrequest, resp)
		return
	}
	if req.KeyId == "" {
		errInfo := fmt.Sprintf("keyId不能为空")
		resp.Err = errInfo
		app.ResponseJson(badrequest, resp)
		return
	}
	var ret dataStruct.TracedDataDigest
	result, err := client.Fabric.QueryKey(req.KeyId)
	if err != nil {
		resp.Err = "内部错误"
		fmt.Println("querykey 内部错误")
		app.ResponseJson(badrequest, resp)
		return
	}
	if len(result) == 0 || result == nil {
		resp.Err = "keyId不存在"
		app.ResponseJson(badrequest, resp)
		return
	}
	for {
		var data dataStruct.DataDigest
		dataByte, err := client.Fabric.QueryKey(req.KeyId)
		err = json.Unmarshal(dataByte, &data)
		if err != nil {
			fmt.Println("loadreceipt,内部反序列化结构体出错")
			resp.Err = "内部出错"
			app.ResponseJson(badrequest, resp)
			return
		}
		ret.TracedList = append(ret.TracedList, data)
		if data.ParentKeyId == "" {
			break
		}
		req.KeyId = data.ParentKeyId
	}
	app.ResponseJson(success, ret)
}
func (app *Application) VerifyFile() {
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var req dataStruct.VerifyRequest
	resp := dataStruct.Response{}
	resp.Success=false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = fmt.Sprintf("数据格式错误")
		app.ResponseJson(badrequest, resp)
		return
	}
	var dataDigest dataStruct.DataDigest
	result, err := client.Fabric.Query("query", []byte(req.KeyId))
	if err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	if len(result) == 0 || result == nil {
		resp.Err = "keyId不存在"
		app.ResponseJson(badrequest, resp)
		return
	}
	errUnmarshal := json.Unmarshal(result, &dataDigest)
	if errUnmarshal != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}

	fmt.Println("verifyFile get data:", string(result))
	if dataDigest.FileHash != req.FileHash {
		resp.Success=false
		resp.Err = "Hash验证不一致"
		app.ResponseJson(badrequest, resp)
		return
	}
	resp.Success=true
	resp.Err = ""
	app.ResponseJson(success, resp)
}
func (app *Application) VerifyChain() {
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var req dataStruct.Request
	resp := dataStruct.ResponseVerify{}
	resp.Success = false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	keyId := req.KeyId
	for i := len(keyId) - 1; i >= 0; i-- {
		var data dataStruct.DataDigest
		dataByte, err := client.Fabric.QueryKey(keyId[i])
		if len(dataByte) == 0 {
			resp.Err = "节点验证失败"
			tmp := dataStruct.MismatchedSet{
				Msg:   "节点不存在",
				KeyId: keyId[i],
			}
			resp.Set = append(resp.Set, tmp)
			continue
		}
		err = json.Unmarshal(dataByte, &data)
		if err != nil {
			fmt.Println("内部反序列化结构体出错")
			resp.Err = "内部出错"
			app.ResponseJson(badrequest, resp)
			return
		}

		if i > 0 && data.ParentKeyId != keyId[i-1] {
			s := fmt.Sprintf("%s 父节点验证失败", data.KeyId)
			tmp := dataStruct.MismatchedSet{
				Msg:   s,
				KeyId: keyId[i],
			}
			resp.Set = append(resp.Set, tmp)
		}
	}
	resp.Success = true
	app.ResponseJson(success, resp)
}
func (app *Application) Default(){
	app.ResponseJson(badrequest,"访问路径错误")
}
func (app *Application) VerifyTree() {
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var req dataStruct.ChainSet
	resp := &dataStruct.ResponseVerify{}
	resp.Success = false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	if len(req.KeySet) == 0 {
		resp.Err = "数据为空"
		app.ResponseJson(badrequest, resp)
	}
	rootkey := req.KeySet[0][0]
	for _, keyid := range req.KeySet {
		if len(keyid) == 0 {
			continue
		}
		if keyid[0] != rootkey {
			s := fmt.Sprintf("%s 不一致", keyid[0])
			resp.Err = "节点验证失败"
			tmp := dataStruct.MismatchedSet{
				Msg:   s,
				KeyId: keyid[0],
			}
			resp.Set = append(resp.Set, tmp)
			continue
		}
		client.Fabric.VerifyChain(keyid, resp)
	}
	resp.Success = true
	app.ResponseJson(success, resp)
}
