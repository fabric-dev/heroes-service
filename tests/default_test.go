package test

import (
	"bytes"
	"encoding/json"
	"heroes-service/dataStruct"
	_ "heroes-service/routers"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
)

func init() {
	_, file, _, _ := runtime.Caller(0)
	apppath, _ := filepath.Abs(filepath.Dir(filepath.Join(file, ".."+string(filepath.Separator))))
	beego.TestBeegoInit(apppath)
}

// TestBeego is a sample to run an endpoint test
func TestBeego(t *testing.T) {
	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)

	beego.Trace("testing", "TestBeego", "Code[%d]\n%s", w.Code, w.Body.String())

	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
	})
}

var rooturl string = "http://127.0.0.1:8080/"
var contentType string = "application/json;charset=utf-8"

func TestStoreReceipt(t *testing.T) {
	url := rooturl + "storeReceipt"
	testData := [][]byte{
		[]byte(`{"keyid":"123","version":"v1.0","username":"user1","uri":"localhost/user1","fileHash":"A123","parentKeyId":""}`),
		[]byte(`{"keyid":"456","version":"v1.1","username":"user2","uri":"localhost/user2","fileHash":"A456","parentKeyId":"123"}`),
	}
	var result dataStruct.BlockInfoResp
	testCommon(t, url, testData, &result)
	if result.Status != "Success" {
		t.Error("failed:", result.Err)
	} else {
		t.Log("txid:", result.TxId)
		t.Log("blockchain:", result.BlockHash)
		t.Log("blockheight:", result.BlockHeight)
	}

}
func testCommon(t *testing.T, url string, testData [][]byte, result interface{}) {
	for _, req := range testData {
		b := bytes.NewBuffer(req)
		resp, err := http.Post(url, contentType, b)
		if err != nil {
			t.Error("Post failed:", err)
			return
		}
		defer resp.Body.Close()
		content, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Error("Read failed:", err)
			return
		}
		json.Unmarshal(content, result)
	}
}
func TestLoadReceipt(t *testing.T) {
	url := rooturl + "loadReceipt"
	testData := [][]byte{
		[]byte(`{"keyId":"456","hash":""}`),
		[]byte(`{"keyId":"123","hash":""}`),
	}
	var result dataStruct.DataDigest
	testCommon(t, url, testData, &result)
	t.Log(result)
}
func TestVerifyFile(t *testing.T) {
	url := rooturl + "verifyFile"
	testData := [][]byte{
		[]byte(`{"keyId":"456","hash":"A45"}`),
		[]byte(`{"keyId":"123","hash":"A123"}`),
	}
	var result dataStruct.Response
	testCommon(t, url, testData, &result)
	if result.Status != "Success" {
		t.Error("failed:", result.Err)
	}
}
func TestVerifyChain(t *testing.T) {
	url := rooturl + "verifyChain"
	testData := [][]byte{
		[]byte(`{"keyId":["123","456"]}`),
	}
	var result dataStruct.Response
	testCommon(t, url, testData, &result)
	if result.Status != "Success" {
		t.Error("failed:", result.Err)
	}
}
