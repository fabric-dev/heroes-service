package blockchain

import (
	"errors"
	"fmt"
	"time"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
)

// InvokeHello
func (setup *FabricSetup) InvokeData(value []byte) ([]byte, error) {

	// Prepare arguments
	var args []string
	args = append(args, "invoke")
	args = append(args, "storeDigest")
	eventID := "eventInvoke"
	// Add data that will be visible in the proposal, like a description of the invoke request
	transientDataMap := make(map[string][]byte)
	transientDataMap["result"] = []byte("Transient data :add key-value ")

	reg, notifier, err := setup.event.RegisterChaincodeEvent(setup.ChainCodeID, eventID)
	if err != nil {
		fmt.Println(err)
		return nil, errors.New("内部出错")
	}
	defer setup.event.Unregister(reg)
	// Create a request (proposal) and send it
	fmt.Println("create a store request.....")
	response, err := setup.client.Execute(channel.Request{ChaincodeID: setup.ChainCodeID, Fcn: args[0], Args: [][]byte{[]byte(args[1]), value}, TransientMap: transientDataMap})
	if err != nil {
		fmt.Printf("failed to move funds: %v\n", err)
		return nil, errors.New("内部出错")
	}

	// Wait for the result of the submission
	select {
	case ccEvent := <-notifier:
		fmt.Printf("Received CC event: %v\n", ccEvent)
	case <-time.After(time.Second * 20):
		fmt.Printf("did NOT receive CC event for eventId(%s)\n", eventID)
		return nil, errors.New("内部出错")
	}

	return []byte(response.TransactionID), nil
}
