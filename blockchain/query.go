package blockchain

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"heroes-service/dataStruct"
	"strconv"
	"log"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"github.com/pkg/errors"
)

// QueryHello query the chaincode to get the state of hello
func (setup *FabricSetup) Query(funcName string, data []byte) ([]byte, error) {

	// Prepare arguments
	var args []string
	args = append(args, "invoke")
	args = append(args, funcName)
	fmt.Printf("query data before:%s\n", string(data))
	response, err := setup.client.Query(channel.Request{ChaincodeID: setup.ChainCodeID, Fcn: args[0], Args: [][]byte{[]byte(args[1]), data}})
	if err != nil {
		return nil, fmt.Errorf("failed to query: %v", err)
	}

	return response.Payload, nil
}
func (setup *FabricSetup) QueryKey(key string) ([]byte, error) {
	return setup.Query("query", []byte(key))
}
func (setup *FabricSetup) VerifyChain(keyId []string, resp *dataStruct.ResponseVerify) {
	var args []string
	args = append(args, "invoke")
	args = append(args, "query")
	response, err := setup.client.Query(channel.Request{ChaincodeID: setup.ChainCodeID, Fcn: args[0], Args: [][]byte{[]byte(args[1]), []byte(keyId[len(keyId)-1])}})
	if err != nil {
		resp.Err = "内部错误"
		return
	}
	if response.Payload == nil || len(response.Payload) == 0 {
		errors.New("叶节点不存在")
	}
	for i := len(keyId) - 1; i >= 0; i-- {
		var dataDigest dataStruct.DataDigest
		response, err = setup.client.Query(channel.Request{ChaincodeID: setup.ChainCodeID, Fcn: args[0], Args: [][]byte{[]byte(args[1]), []byte(keyId[i])}})
		if err != nil {
			resp.Err = "内部错误"
			log.Println("VerifyChain marshall err")
			return
		}
		errUnmarshal := json.Unmarshal(response.Payload, &dataDigest)
		if errUnmarshal != nil {
			resp.Err = "内部错误"
			log.Println("VerifyChain unmarshall err")
			return
		}
		if len(response.Payload) == 0 {
			resp.Err = "节点验证失败"
			tmp := dataStruct.MismatchedSet{
				Msg:   "节点匹配失败",
				KeyId: keyId[0],
			}
			resp.Set = append(resp.Set, tmp)
			continue
		}
		if i > 0 && dataDigest.ParentKeyId != keyId[i-1] {
			s := fmt.Sprintf("%s 父节点验证失败", dataDigest.KeyId)
			resp.Err = "节点验证失败"
			tmp := dataStruct.MismatchedSet{
				Msg:   s,
				KeyId: keyId[0],
			}
			resp.Set = append(resp.Set, tmp)
		}
	}
}
func (setup *FabricSetup) GetBlockHeaderInfo(resp *dataStruct.BlockInfoResp, txId []byte) error {
	block, err := setup.ledgerClient.QueryBlockByTxID(fab.TransactionID(txId))
	if err != nil {
		fmt.Printf("failed to query block by transaction ID: %s\n", err)
		return err
	}
	resp.BlockHeight = strconv.Itoa(int(block.Header.GetNumber()))
	fmt.Println("block number: ", block.Header.GetNumber())
	resp.BlockHash = hex.EncodeToString(block.Header.GetDataHash())
	return nil
}
